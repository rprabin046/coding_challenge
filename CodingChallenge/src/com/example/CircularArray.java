package com.example;

import java.util.Iterator;

public class CircularArray{

	private int[] items;
	private int count;
	private int start;
	
	public CircularArray(int start, int length) {
		this.start = start;
		items = new int[length];
	}
	
	public void print() {
		for(int i = this.start; i < (start + count); i++) {
			System.out.print(items[i % items.length] + " ");
		}
		System.out.println();
	}
	
	public int itemAt(int index) {
		return items[index];
	}
	
	public void insert(int item) {
		// If the array is full, resize it
		
		if(items.length == count) {
			int[] newItems = new int[count * 2];
			
			for(int i = 0; i < count; i++) {
				newItems[i] = items[i];
			}
			items = newItems;
		}
		items[(start + count) % items.length] = item;
		count++;
	
	}
	
	public void insertAt(int pos, int item) {
		int index = (start + pos) % items.length;
		if(index < 0 || index >= items.length) {
			throw new IllegalArgumentException();
		}else {
			// Find the number of elements to shift
			int nShifts = count - pos;
			// Find the index of element need to be moved, in first case it will be last element
			int from = (start + count - 1) % items.length;
			// Find the destination index
			int to = (from + 1) % items.length;
			for(int i = 0; i < nShifts; i++) {
				items[to] = items[from];
				
				//go for next item
				to = from;
				from = from - 1;
				if(from == -1) {
					from = count;
				}
			}
			items[index] = item;
			
		}
	}
	
	
	public void removeAt(int pos) {
		int index = (start + pos) % items.length;
		if(index < 0 || index >= items.length) {
			throw new IllegalArgumentException();
		}else {
			int nShifts = count - pos;
			
			int to = index;
			int from = (to + 1) % items.length;
			for(int i = 0; i < nShifts; i++) {
				items[to] = items[from];
				
				//go for next item
				to = from;
				from = (from +1) % items.length;
			}
			items[(start + count) % items.length] = 0;
			count--;
		}
	}
	
	
	
	public int size() {
		return items.length;
	}

	
	
	
	
}
