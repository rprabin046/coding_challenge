package com.example;

public class MainDriver {
	public static void main(String[] args) {
		CircularArray arr = new CircularArray(5,8);
		//Inserting Elements
		arr.insert(5);
		arr.insert(7);
		arr.insert(9);
		arr.insert(15);
		arr.insert(-4);
		arr.insert(17);
		
		//Printing Circular Array
		arr.print();
		
		//Printing in Linear format for testing
		display(arr);
		
		//Adding new Element 
		arr.insert(12);
		display(arr);
		
		//Adding new Element at specific index which then move the rest of the elements to right
		arr.insertAt(4, 2);
		display(arr);
		
		//Removing element at specific index
		arr.removeAt(5);
		display(arr);
		
		
		

	}
	public static void display(CircularArray arr) {
		System.out.println("\n\nPrinting Linearly");
		for(int i = 0; i < arr.size(); i++) {
			System.out.print(arr.itemAt(i) + " ");
		}
	}
}